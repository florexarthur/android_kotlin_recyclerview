package ec.edu.recyclerexample

import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(val nombre: String, val apellido: String, val escovid: Boolean):Parcelable

class AdaptableUser(
    private val datos: MutableList<User>,
    private val clickListener: (User) -> Unit):
    RecyclerView.Adapter<AdaptableUser.UserViewHolder>(){

    class UserViewHolder(val item:View):RecyclerView.ViewHolder(item){
        val lblNombre = item.findViewById(R.id.lblNombre) as TextView
        val lblApellido = item.findViewById(R.id.lblApellido) as TextView
        val lblEsCovid = item.findViewById(R.id.lblCovid) as TextView
        fun bindUser(user: User){
            lblApellido.text = user.apellido
            lblNombre.text = user.nombre
            if (user.escovid) {
                lblApellido.setBackgroundResource(android.R.color.holo_red_light)
                lblNombre.setBackgroundResource(android.R.color.holo_red_light)
                lblEsCovid.setBackgroundResource(android.R.color.holo_red_light)
                lblEsCovid.text = "Covid positivo"
            }
            else
                lblEsCovid.text = "Covid Negativo"
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val item = LayoutInflater.from(parent.context).
        inflate(R.layout.listitem_user, parent, false) as LinearLayout
        return UserViewHolder(item)
    }

    override fun getItemCount()= datos.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = datos[position]
        holder.bindUser(user)
        holder.item.setOnClickListener{clickListener(user)};
    }
}