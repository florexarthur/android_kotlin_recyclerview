package ec.edu.recyclerexample

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class registry : AppCompatActivity() {

    private lateinit var txtName: EditText
    private lateinit var txtLastname: EditText
    private lateinit var txtAge: EditText
    private lateinit var btnAdd: Button

    private lateinit var chkHabitual1: CheckBox
    private lateinit var chkHabitual2: CheckBox
    private lateinit var chkHabitual3: CheckBox
    private lateinit var chkMenosComunes1: CheckBox
    private lateinit var chkMenosComunes2: CheckBox
    private lateinit var chkMenosComunes3: CheckBox
    private lateinit var chkMenosComunes4: CheckBox
    private lateinit var chkMenosComunes5: CheckBox
    private lateinit var chkMenosComunes6: CheckBox
    private lateinit var chkMenosComunes7: CheckBox
    private lateinit var chkSintomaGrave1: CheckBox
    private lateinit var chkSintomaGrave2: CheckBox
    private lateinit var chkSintomaGrave3: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registry)

        txtName = findViewById(R.id.txtName)
        txtLastname = findViewById(R.id.txtLastName)
        txtAge = findViewById(R.id.txtAge)
        btnAdd = findViewById(R.id.btnAdd)
        chkHabitual1 = findViewById(R.id.chkHabitual1)
        chkHabitual2 = findViewById(R.id.chkHabitual2)
        chkHabitual3 = findViewById(R.id.chkHabitual3)
        chkMenosComunes1 = findViewById(R.id.chkMenosComunes1)
        chkMenosComunes2 = findViewById(R.id.chkMenosComunes2)
        chkMenosComunes3 = findViewById(R.id.chkMenosComunes3)
        chkMenosComunes4 = findViewById(R.id.chkMenosComunes4)
        chkMenosComunes5 = findViewById(R.id.chkMenosComunes5)
        chkMenosComunes6 = findViewById(R.id.chkMenosComunes6)
        chkMenosComunes7 = findViewById(R.id.chkMenosComunes7)
        chkSintomaGrave1 = findViewById(R.id.chkSintomaGrave1)
        chkSintomaGrave2 = findViewById(R.id.chkSintomaGrave2)
        chkSintomaGrave3 = findViewById(R.id.chkSintomaGrave3)

        var listDatos = arrayListOf<User>()
        var menos_comunes = 0
        var esCovid = false

        fun validarCovid() {
            val listMenosComunes = arrayOf(
                chkMenosComunes1,
                chkMenosComunes2,
                chkMenosComunes3,
                chkMenosComunes4,
                chkMenosComunes5,
                chkMenosComunes6,
                chkMenosComunes7
            )
            for (i in 0..listMenosComunes.size - 1) {
                if (listMenosComunes[i].isChecked) {
                    menos_comunes += 1
                }
            }
            if (chkHabitual1.isChecked and chkHabitual2.isChecked and chkHabitual3.isChecked) {
                esCovid = true
            } else if (menos_comunes > 2) {
                esCovid = true
            } else if (chkSintomaGrave1.isChecked or chkSintomaGrave2.isChecked or chkSintomaGrave3.isChecked) {
                esCovid = true
            } else {
            }
        }

        val bundle = this.intent.extras
        if (bundle != null) {
            listDatos = bundle.getSerializable("listDatos") as ArrayList<User>
        }

        btnAdd.setOnClickListener{
            validarCovid()
            listDatos.add(User( txtName.text.toString(), txtLastname.text.toString(), esCovid))
            val frm2 = Intent(this@registry, MainActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable("listDatos", listDatos)
            frm2.putExtras(bundle)
            startActivity(frm2)
        }
    }
}