package ec.edu.recyclerexample

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.*

class MainActivity : AppCompatActivity() {
    private lateinit var btnAdd: Button
    private lateinit var btnDel: Button
    private lateinit var btnMove: Button
    private lateinit var recView: RecyclerView
    private lateinit var lnlUser: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnAdd = findViewById(R.id.btnAdd1)
        btnDel = findViewById(R.id.btnDel1)
        recView = findViewById(R.id.recView1)

        val datos = MutableList(0) { i -> User("Nombre $i", "Apellido $i", false) }
        var listDatos = arrayListOf<User>()

        val adaptable = AdaptableUser(datos) {
            Log.i("DemoRecycleView", "Click en ${it.nombre} ")
        }

        recView.setHasFixedSize(true)

        //1. Linear Layout Manager
        recView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        //2. Grid Layout Manager
        //recView.layoutManager = GridLayoutManager(this, 3)

        recView.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        recView.itemAnimator = DefaultItemAnimator()
        recView.adapter = adaptable

        btnAdd.setOnClickListener {
            val frm2 = Intent(this@MainActivity, registry::class.java)
            val bundle = Bundle()
            listDatos.clear()
            for (user in datos)
                listDatos.add(user)
            bundle.putSerializable("listDatos", listDatos)
            frm2.putExtras(bundle)
            startActivity(frm2)
        }

        val bundle = this.intent.extras
        if (bundle != null) {
            listDatos = bundle.getSerializable("listDatos") as ArrayList<User>
            for (user in listDatos)
                datos.add(user)
            adaptable.notifyItemInserted(1)
        }

        btnDel.setOnClickListener {
            var cantidad_user = datos.count()
            if (cantidad_user > 0)
                datos.removeAt(0)
            adaptable.notifyItemRemoved(0)
        }

/*       btnMove.setOnClickListener {
            val userAux = datos[1]
            datos[1] = datos[2].also { datos[2] = datos[1] }
            adaptable.notifyItemMoved(1, 2)
        }
*/
    }

}