package ec.edu.recyclerexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.recyclerview.widget.*

class MainActivity : AppCompatActivity() {
    private lateinit var btnAdd: Button
    private lateinit var btnDel: Button
    private lateinit var btnMove: Button
    private lateinit var recView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnAdd = findViewById(R.id.btnAdd1)
        btnDel = findViewById(R.id.btnDel1)
        btnMove = findViewById(R.id.btnMove1)
        recView = findViewById(R.id.recView1)


        val datos = MutableList(5){i-> User("Nombre $i","Apellido $i")}

        val adaptable = AdaptableUser(datos){
            Log.i("DemoRecycleView","Click en ${it.nombre} ")
        }

        recView.setHasFixedSize(true)

        //1. Linear Layout Manager
        recView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        //2. Grid Layout Manager
        //recView.layoutManager = GridLayoutManager(this, 3)

        recView.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        recView.itemAnimator = DefaultItemAnimator()

        recView.adapter = adaptable

        btnAdd.setOnClickListener {
            datos.add(1, User("Nombre new", "Apellido new"))
            adaptable.notifyItemInserted(1)
        }

        btnDel.setOnClickListener {
            datos.removeAt(1)
            adaptable.notifyItemRemoved(1)
        }

        btnMove.setOnClickListener {
            val userAux = datos[1]
            datos[1] = datos[2].also { datos[2] = datos[1] }
            adaptable.notifyItemMoved(1, 2)
        }


    }
}
